
let
  waybarStyle = "slickbar-num"; # simplebar, slickbar, slickbar-num, or default
in {
  theme = "dracula";
  slickbar = if waybarStyle == "slickbar" then true else false;
  slickbar-num = if waybarStyle == "slickbar-num" then true else false;
  simplebar = if waybarStyle == "simplebar" then true else false;
  borderAnim = true;
  clock24h = false;

  # Enable Printer & Scanner Support
  printer = false;

  # Enable Flatpak & Larger Programs
  distrobox = false;
  flatpak = false;
  kdenlive = true;
  blender = true;

  # Enable Support For
  # Logitech Devices
  logitech = true;

  # Enable Terminals
  # If You Disable All You Get Kitty
  wezterm = true;
  alacritty = false;
  kitty = true;

  # Enable Python & PyCharm
  python = false;
}

{ gtkThemeFromScheme, config, pkgs, inputs, gitEmail, gitUsername, username, ... }:

let

  inherit (import ./options.nix) theme;

in
{
  home.username = "${username}";
  home.homeDirectory = "/home/${username}";
  home.stateVersion = "23.11"; # Please read the comment before changing.

  colorScheme = inputs.nix-colors.colorSchemes."${theme}";

  imports = [
    ./user
   inputs.nix-colors.homeManagerModules.default
   inputs.nixvim.homeManagerModules.nixvim
   inputs.hyprland.homeManagerModules.default
  ];

  # Define Settings For Xresources
  xresources.properties = {
    "Xcursor.size" = 24;
  };

    # Install & Configure Git
  programs.git = {
    enable = true;
    userName = "${gitUsername}";
    userEmail = "${gitEmail}";
  };


  # Create XDG Dirs
  xdg = {
    userDirs = {
        enable = true;
        createDirectories = true;
    };
  };

  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = ["qemu:///system"];
      uris = ["qemu:///system"];
    };
  };

  programs.home-manager.enable = true;
}

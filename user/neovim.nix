{ config, pkgs, ... }:

let
  palette = config.colorScheme.palette;
in {
  programs.neovim = {
    enable = true;
    plugins = with pkgs; [

      vimPlugins.nvim-lspconfig
      vimPlugins.oceanic-next
      vimPlugins.which-key-nvim
    ];
  };
}

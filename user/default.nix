{ pkgs, config, ... }:

{
  imports = [
    # Enable &/ Configure Programs
    ./bash.nix
    ./gtk-qt.nix
    ./hyprland.nix
    ./kitty.nix
    ./nixvim.nix
    ./packages.nix
    ./rofi.nix
    ./starship.nix
    ./waybar.nix
    ./swaylock.nix
    ./swaync.nix
    ./wezterm.nix

    # Place Home Files Like Pictures
    ./files.nix
  ];
}

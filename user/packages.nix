{ config, lib, pkgs, ... }:
let
  inherit (import ../options.nix);
in {
  home.packages = with pkgs; [

  (import ../scripts/rofi-launcher.nix { inherit pkgs; })
  (import ../scripts/rewaybar.nix { inherit pkgs; })
  (import ../scripts/sleep.nix { inherit pkgs; })
  (import ../scripts/task-waybar.nix { inherit pkgs; })

  ];

}

{ config, pkgs, ... }:

{
  # Place Files Inside Home Directory
  home.file.".config/rofi/rofi.jpg".source = ./rofi.jpg;
  home.file.".config/starship.toml".source = ./starship.toml;
  home.file.".config/sxhkd/sxhkdrc".source = ./sxhkdrc;
  home.file.".config/doom/" = {
    source = ./doom;
    recursive = true;
  };

}
